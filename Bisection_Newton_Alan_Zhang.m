% Project#3: Bisection VS Newton's Methods to Calculate Implied Volatility
% Alan Zhang -- 5127095

% This project contains two methods to calculate implied volatility using 
% Black-Scholes model  

% The outputs are implied volatility and # of iterations


% Section 1: Inputs

% Inputs for the Black-Scholes model.
S = input('Please enter the Underlying price: ');
K = input('Please enter the Strike price: ');
r= input('Please enter the Risk-free rate(in decimal): ');
T = input('Please enter the Tenor(in year): ');
CP = input('Please enter call(1) or put(2): ');  % Call or Put option

% Input option price matrix
P = input('Please enter the option price: ');


% Section 2: Execute Bisection method and Newton's method
n = length(P);  % Size of option price matrix

% Vectorized implied volatility calculation
[vectorvols_bi, step_b] = arrayfun(@Bisection,S*ones(1,n),K*ones(1,n),r*ones(1,n),T*ones(1,n),P,CP*ones(1,n));
[vectorvols_nt, step_n] = arrayfun(@Newton,S*ones(1,n),K*ones(1,n),r*ones(1,n),T*ones(1,n),P,CP*ones(1,n));

% Outputs
disp(['The implied volatility(Bisection):',num2str(vectorvols_bi)]);
disp(['The number of iterations(Bisection):',num2str(step_b)]);
disp(['The implied volatility(Newton):',num2str(vectorvols_nt)]);
disp(['The number of iterations(Newton):',num2str(step_n)]);


% Section 3: Bisection method function and Newton's method function

% (1) Bisection method
function [iv_bi, stepsize_b] = Bisection(S, K, r, T, P, CP)
% From the first project regarding Black-scholes model
d1 = @(S,K,r,sig,T) (log(S/K)+T*(r+(sig^2)/2))/(sig*sqrt(T));
d2 = @(S,K,r,sig,T) d1(S,K,r,sig,T)-sig*sqrt(T);
Call = @(S,K,r,sig,T)  S*cdf('norm',d1(S,K,r,sig,T),0,1)-K*exp(-r*T)*cdf('norm',d2(S,K,r,sig,T),0,1);
Put = @(S,K,r,sig,T)  K*exp(-r*T)*cdf('norm',-d2(S,K,r,sig,T),0,1)-S*cdf('norm',-d1(S,K,r,sig,T),0,1);

% Sigma upper bound and lower bound
upper = 2.0; 
lower = 0.0; 
sig = 1.0;         % Initial volatility test value
stepsize_b = 0;    % Initial # of iterations
if CP == 1
    while abs(Call(S,K,r,sig,T) - P) > 0.001
    sig = (upper + lower)/2;
        if P > Call(S,K,r,sig,T)
        lower = sig;
        else
        upper = sig;      
        end
        stepsize_b = stepsize_b + 1;
    end
else
    while abs(Put(S,K,r,sig,T) - P) > 0.001
    sig = (upper + lower)/2;
        if P > Put(S,K,r,sig,T)
        lower = sig;
        else
        upper = sig; 
        end
        stepsize_b = stepsize_b + 1;
    end
end    
iv_bi = sig;
end

% (2) Newton's method
function [iv_nt, stepsize_n] = Newton(S, K, r, T, P, CP)
% From the first project regarding Black-scholes model
d1 = @(S,K,r,sig,T) (log(S/K)+T*(r+(sig^2)/2))/(sig*sqrt(T));
d2 = @(S,K,r,sig,T) d1(S,K,r,sig,T)-sig*sqrt(T);
Call = @(S,K,r,sig,T)  S*cdf('norm',d1(S,K,r,sig,T),0,1)-K*exp(-r*T)*cdf('norm',d2(S,K,r,sig,T),0,1);
Put = @(S,K,r,sig,T)  K*exp(-r*T)*cdf('norm',-d2(S,K,r,sig,T),0,1)-S*cdf('norm',-d1(S,K,r,sig,T),0,1);
Vega = @(S,K,r,sig,T)S * normpdf(d1(S,K,r,sig,T)) * sqrt(T);

% Initial values for volatility test and # of iterations
sig = 2.0;
stepsize_n = 0;
if CP == 1
    while (abs(Call(S,K,r,sig,T) - P) > 0.0001)
        deltaX = (P - Call(S,K,r,sig,T))/Vega(S,K,r,sig,T);
        sig = sig + deltaX;
        stepsize_n = stepsize_n + 1;
    end
else
    while (abs(Put(S,K,r,sig,T) - P) > 0.0001)
        deltaX = (P - Put(S,K,r,sig,T))/Vega(S,K,r,sig,T);
        sig = sig + deltaX;
        stepsize_n = stepsize_n + 1;
    end
end
iv_nt = sig;
end



