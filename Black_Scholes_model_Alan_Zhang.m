%Project#1: Black-Scholes pricing Model  (Alan Zhang-5127095)

%This Black-Scholes pricing model has five iputs: underlying price, Strike price, Risk-free rate, Volatility and Tenor(listed in Sec.1).

%The model finally will output values for Call price, Put price, Call delta, Put delta, Gamma, Vega, Call theta, Put theta, Call rho and Put rho.


%Section 1: Inputs

%Five inputs for the Black-Scholes pricing model.
S = input('Please enter the underlying price: ');
K = input('Please enter the strike price: ');
r= input('Please enter the risk-free rate(in decimal): ');
sig = input('Please enter the volatility(in decimal): ');
T = input('Please enter the Tenor(in year): ');


%Section 2: Model Construction

%First, we define functions d1 and d2 for CDF calculations. 
d1 = @(S,K,r,sig,T) (log(S/K)+(r+(sig^2)/2)*T)/(sig*sqrt(T));
d2 = @(S,K,r,sig,T) d1(S,K,r,sig,T)-sig*sqrt(T);

%Next,we define functions for our outputs.
Call_Price = @(S,K,r,sig,T)  S*cdf('Normal',d1(S,K,r,sig,T),0,1)-K*exp(-r*T)*cdf('Normal',d2(S,K,r,sig,T),0,1);
Put_Price = @(S,K,r,sig,T)  K*exp(-r*T)*cdf('Normal',-d2(S,K,r,sig,T),0,1)-S*cdf('Normal',-d1(S,K,r,sig,T),0,1);
Call_Delta = @(S,K,r,sig,T)  cdf('Normal',d1(S,K,r,sig,T),0,1);
Put_Delta = @(S,K,r,sig,T)  cdf('Normal',d1(S,K,r,sig,T),0,1)-1;
Gamma = @(S,K,r,sig,T)  (pdf('Normal',d1(S,K,r,sig,T),0,1))/(S*sig*sqrt(T));
Vega = @(S,K,r,sig,T)  S*pdf('Normal',d1(S,K,r,sig,T),0,1)*sqrt(T);
Call_Theta = @(S,K,r,sig,T)  -(S*pdf('Normal',d1(S,K,r,sig,T),0,1)*sig)/(2*sqrt(T))-r*K*exp(-r*T)*cdf('Normal',d2(S,K,r,sig,T),0,1);
Put_Theta = @(S,K,r,sig,T)  -(S*pdf('Normal',d1(S,K,r,sig,T),0,1)*sig)/(2*sqrt(T))+r*K*exp(-r*T)*cdf('Normal',-d2(S,K,r,sig,T),0,1);
Call_Rho = @(S,K,r,sig,T)  K*T*exp(-r*T)*cdf('Normal',d2(S,K,r,sig,T),0,1);
Put_Rho = @(S,K,r,sig,T)  -K*T*exp(-r*T)*cdf('Normal',-d2(S,K,r,sig,T),0,1);


%Section 3: Output final results

%Display values of outputs.
prompt = 'Please enter the name of output:'; 
str = input(prompt,'s'); %Output the desired value by entering variable names defined in section 2.
switch(str)
    case 'Call_Price'
        disp(['The call price is: ',num2str(Call_Price(S,K,r,sig,T))]);
    case 'Put_Price'      
        disp(['The put price is: ', num2str(Put_Price(S,K,r,sig,T))]);
    case 'Call_Delta' 
        disp(['The call delta is : ', num2str(Call_Delta(S,K,r,sig,T))]);
    case 'Put_Delta'
        disp(['The put delta is : ', num2str(Put_Delta(S,K,r,sig,T))]);
    case 'Gamma'
        disp(['The gamma is : ', num2str(Gamma(S,K,r,sig,T))]);
    case 'Vega'
        disp(['The vega is : ', num2str(Vega(S,K,r,sig,T))]);
    case 'Call_Theta'
        disp(['The call theta is : ', num2str(Call_Theta(S,K,r,sig,T))]);
    case 'Put_Theta'
        disp(['The put theta is : ', num2str(Put_Theta(S,K,r,sig,T))]);
    case 'Call_Rho'
        disp(['The call rho is : ', num2str(Call_Rho(S,K,r,sig,T))]);
    case 'Put_Rho'
        disp(['The put rho is : ', num2str(Put_Rho(S,K,r,sig,T))]);
end