% Project#2: Binomial Tree pricing Model  (Alan Zhang-5127095)

% This Binomial pricing model has seven iputs: Underlying price, Strike price;
% Risk-free rate, Volatility, Tenor, Number of steps and Dividend(listed in Sec.1).

% This pricing model outputs values for Call price, Put price(European or American); 
% And all Greeks associated with the option.


% Section 1: Inputs

% Seven inputs for the Binomial pricing model.
S = input('Please enter the Underlying price: ');
K = input('Please enter the Strike price: ');
r= input('Please enter the Risk-free rate(in decimal): ');
sig = input('Please enter the Volatility(in decimal): ');
T = input('Please enter the Tenor(in year): ');
N = input('Please enter the Number of steps: '); % In order to calculate Greeks, N must be greater than 2.
q = input('Please enter the Dividend: ');

% Two extra inputs to deteremine the type of option and its corresponding;
% binomial tree.

a = input('Please enter "1" for American option or "2" for European option: ');
b = input('Please enter "1" for Put or "2" for Call: ');


% Section 2: Model Construction and Execution

% Use FUNCTION BINOMIAL to caculate the price and all associated GREEKs.
[p1,p2,A,B,C] = Binomial(b,S,K,r,sig,T,N,q);

% (1). Delta
d1 = (B(1,2)-B(2,2))/(A(1,2)-A(2,2));   % Delta for American Option
d2 = (C(1,2)-C(2,2))/(A(1,2)-A(2,2));   % Delta for European Option

% (2). Gamma
g1 = ((B(1,3)-B(2,3))/(A(1,3)-A(2,3))-((B(2,3)-B(3,3))/(A(2,3)-A(3,3))))/((1/2)*(A(1,3)-A(3,3)));
% Gamma for American Option
g2 = ((C(1,3)-C(2,3))/(A(1,3)-A(2,3))-((C(2,3)-C(3,3))/(A(2,3)-A(3,3))))/((1/2)*(A(1,3)-A(3,3)));
% Gamma for European Option


% (3) Vega
s1 = sig*(1+0.001);   % new volatilities x,y after a small fraction of change.
s2 = sig*(1-0.001);   % delta sigma change = 0.001sigma
[vp1,vp2]=Binomial(b,S,K,r,s1,T,N,q); % American and European option price for s1.
[vp3,vp4]=Binomial(b,S,K,r,s2,T,N,q); % American and European option price for s2.
v1=(vp1-vp3)/2/(0.001*sig);        % Vega for American option
v2=(vp2-vp4)/2/(0.001*sig);        % Vega for European option

% (4) Theta
dt = T/N;
t1 = (B(2,3)-B(1,1))/(2*dt);   % Theta for American option
t2 = (C(2,3)-C(1,1))/(2*dt);   % Theta for European option

% (5) Rho
% Define new risk-neutral rate
r1 = r*(1+0.001);
r2 = r*(1-0.001);
[rp1,rp2] = Binomial(b,S,K,r1,sig,T,N,q); % American and European option price for r1.
[rp3,rp4] = Binomial(b,S,K,r2,sig,T,N,q); % American and European option price for r2.
rho1 = (rp1-rp3)/(2*0.001*r);  % Rho for American option
rho2 = (rp2-rp4)/(2*0.001*r);  % Rho for European option


%Section 3: Output final results

if a==1 % American option
   disp(['The American option price is:',num2str(p1)]);
   disp(['The Delta is:',num2str(d1)]);
   disp(['The Gamma is:',num2str(g1)]);
   disp(['The Vega is:',num2str(v1)]);
   disp(['The Theta is:',num2str(t1)]);
   disp(['The Rho is:',num2str(rho1)]);
  
else    % European option
   disp(['The European option price is:',num2str(p2)]);
   disp(['The Delta is:',num2str(d2)]);
   disp(['The Gamma is:',num2str(g2)]);
   disp(['The Vega is:',num2str(v2)]);
   disp(['The Theta is:',num2str(t2)]);
   disp(['The Rho is:',num2str(rho2)]);
end


% Declare a function to execute the Binomial tree operation.
function [p1,p2,A,B,C] = Binomial(b,S,K,r,sig,T,N,q)
dt = T/N;                     
u = exp(sig*sqrt(dt));         % underlying price goes up by 
d = 1/u;                       % underlying price goes down by
p = (exp((r-q)*dt)-d)/(u-d);   % Risk-neutral probability

% (1). Building underlying prices matrix
A = zeros(N+1);                % Underlying prices matrix
A(1,1) = S;                    % Initial underlying price
    for j = 2:(N+1)                % Constructing Bionomial Tree(Underlying price matrix) 
        A(1,j) = A(1,j-1)*u;
        for i = 2:j
            A(i,j) = A(i-1,j-1)*d;
        end
    end

% (2). Option values at expiration date(No early execution)
B = zeros(N+1);
B(:,N+1) = max(0, ((-1)^b)*(A(:,N+1)-K)); % Used for American option values matrix later.
% Payoffs at the expiration date.
% Call: b = 2
% Put: b = 1
                                                                          
% (3). Building option values matrix
C = zeros(N+1);         % Define an European option value matrix
C(:,N+1) = B(:,N+1);      
    for j = N:-1:1      % Calculate option values in reverse order
        for i = 1:j
            B(i,j) = max((-1)^b*(A(i,j)-K),exp(-r*dt)*(p*B(i,j+1)+(1-p)*B(i+1,j+1)));   
        end
    end
    p1=B(1,1);          % American option price


    for j = N:-1:1      % Calculate option values in reverse order
        for i = 1:j   
            C(i,j) = exp(-r*dt)*(p*C(i,j+1)+(1-p)*C(i+1,j+1));
        end
    end
    p2=C(1,1);          % European option price
end